import React, {useState}from 'react';
import logo from './logo.svg';
import './App.css';
import Gendergame from "./Gendergame"
import calculateWinner from './calculateWinner';
import getwinnerlocation from './getwinnerlocation';
import locatlistorder from "./locatlistorder"

function Square(props) {
 const winnerlocation=props.winnerlocation===false?[null,null,null]:props.winnerlocation
  console.log(winnerlocation.find(e => e ===props.squarenumber ));
  const iswinnerlocation=winnerlocation.find(e => e ===props.squarenumber )===undefined?false:true
  console.log(props.squarenumber)
  console.log(iswinnerlocation)

 const color=iswinnerlocation===true?"square lightblue":"square white"
  return (
    <button className={color} onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  isWinnerSquare(i) {
    if(this.props.winner && this.props.winner.line.findIndex(el => el === i) !== -1) {
        return true;
    }
    return null;
}

  renderSquare(i) {
    return (
      <Square
      squarenumber={i}
       winnerlocation={this.props.winnerlocation}
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
        winner={ this.isWinnerSquare(i)}
        key={i}
      />
    );
  }
  render() {
    let col=[0,1,2]
    let row=[0,1,2]
    return (
<>
{row.map((row, index) => {
return(
  <div className="board-row" key={row}>
    {
 col.map((col, index) => {
  return(<>{this.renderSquare(row*3+col)}</>)
  })
    }
</div>
)})}
  </>
      // <div>
      //   <div className="board-row">
      //     {this.renderSquare(0)}
      //     {this.renderSquare(1)}
      //     {this.renderSquare(2)}
      //   </div>
      //   <div className="board-row">
      //     {this.renderSquare(3)}
      //     {this.renderSquare(4)}
      //     {this.renderSquare(5)}
      //   </div>
      //   <div className="board-row">
      //     {this.renderSquare(6)}
      //     {this.renderSquare(7)}
      //     {this.renderSquare(8)}
      //   </div>
      // </div>
    );
  }
}

export default class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true,
      lines:[null,null,null],
      declist:true,
      declist:true,
    };
  }
  calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        this.state.lines=lines[i]
        return squares[a];
      }
    }
    return null;
  }

  handleClick(i) {
    console.log(i)
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    console.log(history[history.length - 1])
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    const position = getlocations(i);
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    console.log(calculateWinner(squares) )
    squares[i] = this.state.xIsNext ? "X" : "O";
    
    this.setState({
      history: history.concat([
        {
          squares: squares,
          position:position
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    });
  }
    locatlistorder(props) {
    return("null")
     }
   
  render() {
    console.log(this.state)
    console.log(locatlistorder(this.state.stepNumber))
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    console.log(getwinnerlocation(current.squares)===null?false:getwinnerlocation(current.squares))
    const winner = calculateWinner(current.squares);
    console.log(calculateWinner(current.squares))
    const moves = history.map((step, move) => {
      console.log(this.state.stepNumber)
      const desc = move ?
        'Go to move #' + move+` ( ${history[move].position.join(',')} )` : 
        'Go to game start';
        console.log(move)

      if(move===this.state.stepNumber)
      {
           return (
      <li key={move}>
        <button style={{color:"#ff5c5c" }}onClick={() => this.jumpTo(move)}>
          {desc}
          </button>
      </li>
      );
      }
     else{
       return( <li key={move}>
        <button onClick={() => this.jumpTo(move)}>{desc}</button>
      </li>)
     }
    });
     const moves_order=this.state.declist?moves:moves.reverse()
    let status;
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Next player: " + (this.state.xIsNext ? "X" : "O");
    }
    return (
      <div className="game">
        <div className="game-board">
          <Board
          winnerlocation={getwinnerlocation(current.squares)===null?false:getwinnerlocation(current.squares)}
            squares={current.squares}
            onClick={i => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
  
          <button onClick={() => this.setState({ declist: false})}>
         Ascending
        </button>
          <button onClick={() => this.setState({ declist: true})}>
          Descending
        </button>
          <ol>{ moves_order}</ol>
        </div>
      </div>
    );
  }
}



function getlocations(i) {
  const rowMap = [
      [1, 1], [1, 2], [1, 3],
      [2,2], [2, 2], [2, 3],
      [3, 1], [3,2], [3, 3],
  ]
  return rowMap[i];
}